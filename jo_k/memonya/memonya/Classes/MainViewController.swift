//
//  MainViewController.swift
//  memonya
//
//  Created by Kei on 08/12/2018.
//  Copyright © 2018 J.J. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var _mTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _mTableView.delegate = self
        _mTableView.dataSource = self
        
        _mTableView.register(UINib(nibName: String(describing: MemoListTableViewCell.self), bundle: nil), forCellReuseIdentifier: MemoListTableViewCell.ReuseId)
        
//        _mTableView.allowsSelection = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource & UITableViewDataSourcePrefetching
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    // - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MemoListTableViewCell.Height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InputTextViewController") as? InputTextViewController
  
        self.navigationController?.pushViewController(vc!, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MemoListTableViewCell.ReuseId, for: indexPath) as! MemoListTableViewCell
        return cell
    }
}
