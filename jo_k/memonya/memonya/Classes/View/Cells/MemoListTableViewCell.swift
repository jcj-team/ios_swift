//
//  MemoListTableViewCell.swift
//  memonya
//
//  Created by Kei on 08/12/2018.
//  Copyright © 2018 J.J. All rights reserved.
//

import UIKit

class MemoListTableViewCell: UITableViewCell {

    static let ReuseId = "MemoListCell"
    static let Height: CGFloat = 60.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func draw(_ data: String) {
        
    }
}
